(ns tic.core-test
  (:require [clojure.test :refer :all]
            [tic.core :refer :all]))

(deftest a-test
  (testing "basic equality."
    (is (= 1 1))))

(deftest filled?-test
  (testing "filled? should return true for filled values"
    (is (filled? (add-move new-board 4 :x) 4) true))

  (testing "filled? should return false for empty spot"
    (is (= false (filled? new-board 4)))))

(deftest check-down-test
  (testing "Checking vertical matches should return true")

  (testing "Different vertical values should not return true"
    (is (= false (check-down (add-move (add-move (add-move new-board 4 :y) 1 :x) 7 :x) 1)))))

(deftest check-right-test
  (testing "Checking horizontal matches should return true"
    (is (= true (check-right (add-move (add-move (add-move new-board 2 :x) 1 :x) 3 :x) 1)))))

(deftest check-down-right-test
  (testing "Checking down right should return true for matches"
    (is (= true (check-down-right (add-move (add-move (add-move new-board 5 :x) 1 :x) 9 :x))))))

(deftest check-down-left-test
  (testing "Checking down left should return true for matches"
    (is (= true (check-down-left (add-move (add-move (add-move new-board 3 :x) 5 :x) 7 :x))))))

(deftest board-full?-test
  (testing "A full board should return true"
    (is (=
          true
          (board-full?
            (-> new-board
                (add-move 1 :x)
                (add-move 2 :x)
                (add-move 3 :x)
                (add-move 4 :x)
                (add-move 5 :x)
                (add-move 6 :x)
                (add-move 7 :x)
                (add-move 8 :x)
                (add-move 9 :x)))))))

(deftest check-board-test
  (testing "a vertical match should return true"
    (is (= true (check-board (add-move (add-move (add-move new-board 4 :x) 1 :x) 7 :x))))))

(deftest opposite-val-test
  (testing "a :x should return :o"
    (is (= :o (opposite-val :x))))
  (testing "other values should return x"
    (is (= :x (opposite-val :o)))))

(deftest val->str-test
  (testing "an :x should return \"x\""
    (is (= "x" (val->str :x))))
  (testing "a :y should return \"y\""
    (is (= "o" (val->str :o)))))

(deftest add-move-test
  (testing "adding a move should result in a filled spot"
    (is (= true (filled? (add-move new-board 3 :x) 3))))
  (testing "adding an invalid move should not change the board"
    (is (= new-board (add-move new-board 12 :x)))))

(deftest board-val-test
  (testing "value should return"
    (is (= :x (board-val (add-move new-board 3 :x) 3)))))
