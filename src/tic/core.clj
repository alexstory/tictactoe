(ns tic.core
  (:gen-class))


(def new-board
  "empty board"
  (reduce
    (fn [board value] (assoc board value nil))
    {}
    (range 1 10)))

(defn board-val
  "gets the value from a position"
  [board pos]
  (get board pos))

(defn filled?
  "returns true if [board] has a value at [pos]"
  [board pos]
  (some? (board-val board pos)))

(def valid? (complement filled?))

(defn add-move
  "adds a move to the board"
  [board pos value]
  (if (and (< 0 pos 10) (valid? board pos))
    (assoc board pos value)
    board))

(defn check
  "helper function to see if board has values"
  [board pos offset]
  (and (filled? board pos)
       (filled? board (+ offset pos))
       (filled? board (+ offset offset pos))
       (= (board-val board pos)
          (board-val board (+ offset pos))
          (board-val board (+ offset offset pos)))))

(defn check-down
  "check a vertical column on the [board] starting rith the topmost [pos]ition"
  [board pos]
  (check board pos 3))

(defn check-right
  "check a horizontal column on the [board] starting rith the leftmost [pos]ition"
  [board pos]
  (check board pos 1))

(defn check-down-right
  "check board diagonally starting from top-left"
  [board]
  (check board 1 4))

(defn check-down-left
  "check board diagonally starting from top-right"
  [board]
  (check board 3 2))

(defn check-board
  "returns true if any winning combination is on the board"
  [board]
  (or
    (check-down board 1)
    (check-down board 2)
    (check-down board 3)
    (check-right board 1)
    (check-right board 4)
    (check-right board 7)
    (check-down-left board)
    (check-down-right board)))

(defn board-full?
  "returns true if every spot on board has a value"
  [board]
  (every? some? (map  second board)))

(defn opposite-val
  "switches between x and o"
  [value]
  (if (= value :x) :o :x))

(defn val->str
  "converts value to string"
  [value]
  (if (some? value)
    (name value)
    "-"))

(defn tic-line
  "returns string version of a board row"
  [board pos]
  (str (val->str (get board pos)) " "
       (val->str (get board (inc pos))) " "
       (val->str (get board (inc (inc pos))))))

(defn board->str
  "converts entire board to a string"
  [board]
  (clojure.string/join (map #(tic-line new-board %) [1 4 7]) "\n")
  (str (tic-line board 1) "\n"
       (tic-line board 4) "\n"
       (tic-line board 7) "\n"))

(defn main-loop!
  "main game logic handler -unsafe"
  [init-board init-player]
  (loop [board init-board player init-player]
    (cond
      (check-board board) (do
        (println (board->str board))
        (println (str "Player " (val->str (opposite-val player)) " wins!")))
      (board-full? board) (do
        (println (board->str board))
        (println "no more moves!"))
      :else (do
        (println (board->str board))
        (println "enter your move!")
        (let [input (Integer. (read-line))]
          (if (valid? board input)
              (recur (add-move board input player) (opposite-val player))
              (do
                (println "illegal move!")
                (recur board player))))))))

(defn -main
  [& args]
  (println
    "enter the number for the space you want to fill \n1 2 3\n4 5 6\n7 8 9\n")
  (main-loop! new-board :x))
