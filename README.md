# Tic
[![build status](https://gitlab.com/alexstory/tictactoe/badges/master/build.svg)](https://gitlab.com/alexstory/tictactoe/commits/master)

Practice tic-tac-toe in clojure


## Usage

    $ lein run

## License

Copyright © 2017 Alex Story

Distributed under the Eclipse Public License either version 1.0
